GPP = g++
FLTKDIR     = $(shell fltk-config --includedir)
SOURCEDIR   = ./src
OUTBIN      = PicMe
OUTDIR      = ./build
OPTIMIZEOPT = -fopenmp
LDLIBS      += -L./fl_imgtk/lib -lfltk -lfltk_images -lfl_imgtk

CFLAGS      += -std=c++11
CFLAGS      += -I$(SOURCEDIR) -I$(FLTKDIR) $(LDLIBS)
CFLAGS      += $(OPTIMIZEOPT)

all: clean prepare ${OUTDIR}/${OUTBIN} run
debug: all
prepare:
	@echo "Preparing ..."
	@mkdir -p ${OUTDIR}
${OUTDIR}/${OUTBIN}:
	@$(GPP) ${SOURCEDIR}/*.cpp ${CFLAGS} -o $@
clean:
	@echo "Cleaning built directories ..."
	@rm -rf ${OUTDIR}
run:
	@echo "Executing ..."
	${OUTDIR}/${OUTBIN}
