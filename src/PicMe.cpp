#include "PicMe.hpp"

Fl_Box* PicMe::canvas;
Fl_PNG_Image* PicMe::imageFile;
std::string PicMe::fileName;
float PicMe::degree;
Fl_Scroll* PicMe::scroll;
PicMe::PicMe()
{
    PicMe::degree = 0.0;
    window = new Fl_Window(500, 500, "PicMe");

    // TODO: Implementation of resizable window
    window->resizable(window);

    menuBar = new Fl_Menu_Bar(0, 0, 500, 30, NULL);
    scroll = new Fl_Scroll{0, 75, 495, 420};
    scroll->type(Fl_Scroll::BOTH);
    scroll->box(FL_GTK_DOWN_FRAME);

    PicMe::canvas = new Fl_Box(10, 80, 490, 410);
    PicMe::canvas->box(FL_NO_BOX);
    scroll->add(canvas);
    scroll->end();
    menuBar_AddItems(menuBar);
    toolbar_Body();

    window->show();
}

void PicMe::menuBar_AddItems(Fl_Menu_Bar* menuBar)
{
    menuBar->add("&File/&Open File", "^o", PicMe::openFile_CB);
    // menuBar->add("&File/&Open Folder", "^O", menuBar_CB, 0, FL_MENU_DIVIDER);
    menuBar->add("&Tools/&Rotate Left", "^l", rotateLeft_CB);
    menuBar->add("&Tools/&Rotate Right", "^r", rotateRight_CB, 0, FL_MENU_DIVIDER);
    // menuBar->add("&About", "^a", menuBar_CB, 0);
}

void PicMe::toolbar_Body()
{
    toolbarFrame = new Fl_Box(0, 30, 500, 40);
    toolbarFrame->box(FL_UP_BOX);

    openFile_Button = new Fl_Button(5, 32, 35, 35);
    openFile_Button->label("@filenew");
    openFile_Button->callback(openFile_CB);

    openFolder_Button = new Fl_Button(55, 32, 35, 35);
    openFolder_Button->label("@fileopen");

    saveFile_Button = new Fl_Button(105, 32, 35, 35);
    saveFile_Button->label("@filesave");

    previousFile_Button = new Fl_Button(155, 32, 35, 35);
    previousFile_Button->label("@<-");

    nextFile_Button = new Fl_Button(205, 32, 35, 35);
    nextFile_Button->label("@->");

    rotateLeft_Button = new Fl_Button(255, 32, 35, 35);
    rotateLeft_Button->label("@$reload");
    rotateLeft_Button->callback(rotateLeft_CB);

    rotateRight_Button = new Fl_Button(305, 32, 35, 35);
    rotateRight_Button->label("@reload");
    rotateRight_Button->callback(rotateRight_CB);
}

void PicMe::openFile_CB(Fl_Widget*, void*)
{
    PicMe::fileName = fl_file_chooser("Open Image", "*.png", "/home/oggy/Pictures/");
    PicMe::imageFile = new Fl_PNG_Image(fileName.c_str());
    PicMe::canvas->image(PicMe::imageFile);
    PicMe::canvas->resize(10,80 , imageFile->w(), imageFile->h());
    PicMe::canvas->redraw();
}
void PicMe::rotateLeft_CB(Fl_Widget*, void*)
{
    degree = degree + 90.0;
    if (degree <= 360.0) {
        Fl_RGB_Image* rotatedImage = fl_imgtk::rotatefree(imageFile, degree);
        PicMe::canvas->image(rotatedImage);
        PicMe::canvas->resize(10,80 , rotatedImage->w(), rotatedImage->h());
        PicMe::scroll->redraw();
        if (degree == 360.0)
            degree = 0.0;
    }
}
void PicMe::rotateRight_CB(Fl_Widget*, void*)
{
    degree = degree - 90.0;
    if (degree <= 0.0) {
        Fl_RGB_Image* rotatedImage = fl_imgtk::rotatefree(imageFile, degree);
        PicMe::canvas->image(rotatedImage);
        PicMe::canvas->resize(10,80 , rotatedImage->w(), rotatedImage->h());
        PicMe::scroll->redraw();
        if (degree == 0.0)
            degree = 360.0;
    }
}
