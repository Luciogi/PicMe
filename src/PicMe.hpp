#include "../fl_imgtk/lib/fl_imgtk.h"
#include <FL/Fl.H>
#include <FL/Fl_BMP_Image.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_File_Chooser.H>
#include <FL/Fl_Image.H>
#include <FL/Fl_JPEG_Image.H>
#include <FL/Fl_Menu_Bar.H>
#include <FL/Fl_PNG_Image.H>
#include <FL/Fl_Scroll.H>
#include <FL/Fl_Widget.H>
#include <FL/Fl_Window.H>
#include <string>
class PicMe {
private:
    Fl_Window* window;
    Fl_Menu_Bar* menuBar;
    Fl_Box* toolbarFrame;
    Fl_Button* openFile_Button;
    Fl_Button* openFolder_Button;
    Fl_Button* saveFile_Button;
    Fl_Button* previousFile_Button;
    Fl_Button* nextFile_Button;
    Fl_Button* rotateLeft_Button;
    Fl_Button* rotateRight_Button;
    static Fl_Box* canvas;
    static Fl_Scroll* scroll;
    static Fl_PNG_Image* imageFile;
    static std::string fileName;
    static float degree;

public:
    PicMe();
    static void menuBar_AddItems(Fl_Menu_Bar* menuBar);
    void toolbar_Body();
    static void openFile_CB(Fl_Widget*, void*);
    static void rotateLeft_CB(Fl_Widget*, void*);
    static void rotateRight_CB(Fl_Widget*, void*);
};
